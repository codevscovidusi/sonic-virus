const express = require('express');
const router = express.Router();

const {
  findByCountry,
  findDistanceBetweenString,
  findDirectionBetweenString,
  getWeightedAggDataFromCoords
} = require('../helpers/country')

const { sortByDate } = require('../util/sort');

/* GET country data listing. */
// TODO: To remove => unused
router.get('/:name', function(req, res, next) {
  let { name } = req.params;
  let data = sortByDate(findByCountry(name));
  res.json(data)
});

// TODO: To remove => unused
router.get('/:fromCountry/distance/:toCountry', function(req, res, next) {
  let { fromCountry, toCountry } = req.params;
  let distance = findDistanceBetweenString(fromCountry, toCountry);
  let angle = findDirectionBetweenString(fromCountry, toCountry)
  res.json({from:fromCountry, to:toCountry, distance:distance, angle:angle })
});

/* GET weighted aggregated data from geographical coordinates */
router.get('/:lon/:lat/aggregated', function(req, res, next) {

  const { lat, lon } = req.params
  const aggregated = getWeightedAggDataFromCoords({lat, lon})

  res.json({ data: aggregated})
});

/* GET weighted aggregated data from geographical coordinates and within a radius */
router.get('/:lon/:lat/aggregated/radius/:radius', function(req, res, next) {
  
  const { lat, lon, radius } = req.params
  const aggregated = getWeightedAggDataFromCoords({lat, lon}, radius)
  
  res.json({ data: aggregated})
});

module.exports = router;
