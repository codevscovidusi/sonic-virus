const express = require('express');
const router = express.Router();

const {
    getDailyTotals
} = require('../helpers/countries')

/* GET global daily totals. */
router.get('/daily-totals', function(req, res, next) {
    const daily_totals = getDailyTotals()
    res.json({ daily_totals })
});

/* GET daily totals within the radius. */
router.get('/:lon/:lat/daily-totals/:radius', function(req, res, next) {
    
    const { lat, lon, radius } = req.params
    const daily_totals = getDailyTotals({lat, lon}, radius)
    res.json({ daily_totals })
});

module.exports = router;
