

module.exports.doDailyIncremental = function (country_index) {
    let windowSize = 1;

    country_index.forEach((val, key) => {
        let sorted_objects = val.sort((a, b) => {
            return a.date - b.date
        })

        let dailyValues = sorted_objects.map((v,i, arr) => {
            const {country, date, raw: {confirmed,deaths,recovered}} = v;
            if (i === 0) {
                v['daily']= {
                    confirmed: arr[i].raw.confirmed ,
                    deaths: arr[i].raw.deaths,
                    recovered: arr[i].raw.deaths ,
                };
                return v;
            } else {
                //todo get previous day an not arr[i-1] since days might be different
                let confirmedClean = (confirmed - arr[i - 1].raw.confirmed >= 0) ? confirmed - arr[i - 1].raw.confirmed : 0;
                let deathsClean = (deaths - arr[i - 1].raw.deaths >= 0) ?  deaths - arr[i - 1].raw.deaths : 0;
                let recoveredClean = (recovered - arr[i - 1].raw.recovered >= 0) ? recovered - arr[i - 1].raw.recovered : 0;
                v['daily']= {
                    confirmed: confirmedClean,
                    deaths: deathsClean,
                    recovered: recoveredClean,
                };
                return v;
            }
        })

        const bucketedValues = []
        for(let i = windowSize; i < dailyValues.length; ++i) {
            let sumConfirmed = 0
            let sumDead = 0
            let sumRecovered = 0

            for (let j = 0; j < windowSize; ++j){
                sumConfirmed += dailyValues[i - j].daily.confirmed
                sumDead += dailyValues[i - j].daily.deaths
                sumRecovered += dailyValues[i - j].daily.recovered
            }


            bucketedValues.push({
                confirmed: sumConfirmed,
                deaths: sumDead,
                recovered: sumRecovered
            })
        }

        const maxConfirmed = (Math.max(...bucketedValues.map(v => Math.max(v.confirmed))) !== 0) ? Math.max(...bucketedValues.map(v => Math.max(v.confirmed))): 1;
        const maxDead = (Math.max(...bucketedValues.map(v => Math.max(v.deaths))) !== 0) ? Math.max(...bucketedValues.map(v => Math.max(v.deaths))): 1;
        const maxRecovered = (Math.max(...bucketedValues.map(v => Math.max(v.recovered))))? Math.max(...bucketedValues.map(v => Math.max(v.recovered))): 1;

        const normalizedPerCategory = dailyValues.map((v,i,arr) => {
            const {country, date, daily:{confirmed,deaths,recovered} } = v;
            v['norm'] = {
                confirmed: Number.parseFloat((confirmed/maxConfirmed).toFixed(2)),
                deaths: Number.parseFloat((deaths/maxDead).toFixed(2)),
                recovered: Number.parseFloat((recovered/maxRecovered).toFixed(2)),
            }
            return v;
        });
    });
}
