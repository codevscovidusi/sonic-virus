const csv = require('async-csv');
const fs = require('fs').promises;

const CONFIRMED_CSV = "./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv"
const RECOVERED_CSV = "./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Recovered.csv"
const DEAD_CSV = "./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Deaths.csv"

const COUNTRY = "Switzerland"
async function readCSV(file, country) {
    const csvString = await fs.readFile(file, 'utf-8');
    const rows = await csv.parse(csvString, {columns: true});
    for(let i = 0; i < rows.length; ++i) {
        if(rows[i]["Country/Region"] ===country) {
            return rows[i];
        }
    }
}


const confirmedP = readCSV(CONFIRMED_CSV, COUNTRY)
const deadP = readCSV(DEAD_CSV, COUNTRY)
const recoveredP = readCSV(RECOVERED_CSV, COUNTRY)

function doCumulative() {
    Promise.all([confirmedP, deadP, recoveredP])
    .then(([confirmed, dead, recovered]) => {
        const dateKeys = Object.keys(confirmed)
            .filter((k) => /^\d+\/\d+\/\d+/.test(k))

        const values = dateKeys.map((date) => {
            return {
                "date" : date,
                "confirmed": parseInt(confirmed[date]),
                "dead": parseInt(dead[date]),
                "recovered": parseInt(recovered[date])
            };
        });
        const maxConfirmed = Math.max(...values.map(v => Math.max(v.confirmed)))
        const maxDead = Math.max(...values.map(v => Math.max(v.dead)))
        const maxRecovered = Math.max(...values.map(v => Math.max(v.recovered)))

        const max = Math.max(maxConfirmed, maxDead, maxRecovered)
        fs.writeFile(COUNTRY + "-cumulative.json", JSON.stringify(values, null, 2))

        const normalizedValues = values.map(({ date, confirmed, dead, recovered }) => {
            return {
                date,
                confirmed: confirmed/max,
                dead: dead/max,
                recovered: recovered/max,
            };
        });
        fs.writeFile(COUNTRY + "-cumulative-normalized.json", JSON.stringify(normalizedValues, null, 2))

        const normalizedPerCategory = values.map(({ date, confirmed, dead, recovered }) => {
            return {
                date,
                confirmed: confirmed/maxConfirmed,
                dead: dead/maxDead,
                recovered: recovered/maxRecovered,
            };
        });

        fs.writeFile(COUNTRY + "-cumulative-normalized-per-category.json", JSON.stringify(normalizedPerCategory, null, 2))

    })

}

const valuesToDaily = (data) => data.map((v, i, arr) => {
    const { confirmed, dead, recovered, date } = v;
    if (i === 0) {
        return v;
    } else {
        return {
            date,
            confirmed: confirmed - arr[i - 1].confirmed,
            dead: dead - arr[i - 1].dead,
            recovered: recovered - arr[i - 1].recovered,
        };
    }

});

function doDailyIncremental(windowSize) {
    Promise.all([confirmedP, deadP, recoveredP])
    .then(([confirmed, dead, recovered]) => {
        const dateKeys = Object.keys(confirmed)
            .filter((k) => /^\d+\/\d+\/\d+/.test(k))
        const values = dateKeys.sort((a,b) => {return new Date(a) - new Date(b)}).map((date) => {
            return {
                "date" : date,
                "confirmed": parseInt(confirmed[date]),
                "dead": parseInt(dead[date]),
                "recovered": parseInt(recovered[date])
            };
        });

        const dailyValues = valuesToDaily(values);

        const bucketedValues = []
        for(let i = windowSize; i < dailyValues.length; ++i) {
            let sumConfirmed = 0
            let sumDead = 0
            let sumRecovered = 0

            for (let j = 0; j < windowSize; ++j){
                sumConfirmed += dailyValues[i - j].confirmed
                sumDead += dailyValues[i - j].dead
                sumRecovered += dailyValues[i - j].recovered
            }

            let averageConfirmed = sumConfirmed / windowSize
            let averageDead = sumDead / windowSize
            let averageRecovered = sumRecovered / windowSize
            let date = dailyValues[i].date;

            bucketedValues.push({
                date,
                "confirmed": sumConfirmed,
                "dead": sumDead,
                "recovered": sumRecovered
            })
        }
        let s = windowSize > 1 ? `-sliding-window-${windowSize}-days` : ""

        fs.writeFile(`${COUNTRY}-daily-incremental${s}.json`, JSON.stringify(bucketedValues))

        const maxConfirmed = Math.max(...bucketedValues.map(v => Math.max(v.confirmed)))
        const maxDead = Math.max(...bucketedValues.map(v => Math.max(v.dead)))
        const maxRecovered = Math.max(...bucketedValues.map(v => Math.max(v.recovered)))

        const max = Math.max(maxConfirmed, maxDead, maxRecovered)

        console.log(maxConfirmed, maxDead, maxRecovered)
        const normalizedValues = bucketedValues.map(({ date, confirmed, dead, recovered }) => {
            return {
                date,
                confirmed: confirmed/max,
                dead: dead/max,
                recovered: recovered/max,
            };
        });

        fs.writeFile(`${COUNTRY}-daily-incremental${s}-normalized.json`, JSON.stringify(normalizedValues))

        const normalizedPerCategory = bucketedValues.map(({ date, confirmed, dead, recovered }) => {
            return {
                date,
                confirmed: confirmed/maxConfirmed,
                dead: dead/maxDead,
                recovered: recovered/maxRecovered,
            };
        });

        fs.writeFile(`${COUNTRY}-daily-incremental${s}-normalized-per-category.json`, JSON.stringify(normalizedPerCategory, function(key, val) {
            return val.toFixed ? Number(val.toFixed(3)) : val;
        }, 2))
    })
}

doCumulative()
doDailyIncremental(1)
doDailyIncremental(3)
doDailyIncremental(5)
