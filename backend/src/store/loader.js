const path = require("path");
const fs = require('fs');
const csv = require('csv');
const { doDailyIncremental } = require('./normalization');
const { PROJECT_DIR } = require('../../settings');
const moment = require('moment')

const data_folder = path.resolve(PROJECT_DIR, "COVID-19/csse_covid_19_data/csse_covid_19_time_series/")

let date_index;
let country_index;


function init(){
    date_index = new Map();
    country_index = new Map();

    let files = fs.readdirSync(data_folder);
    files = files.filter(file => file.endsWith("global.csv"));

    let current = 0
    const walk_done = () => {
        current++
        if (current === files.length) {
            doDailyIncremental(country_index)
            console.log('finished loading')
        }
    }

    files.forEach((file) => {
        processCSVFile(file, walk_done)
    })
}

function processCSVFile(file, done) {
    const csv_path = data_folder+'/'+file;
    const csvString = fs.readFileSync(csv_path, 'utf-8');
    csv.parse(csvString, {columns: true},function (err, rows) {
        const category = getCaseCategory(csv_path)
        
        rows.forEach((row) => processRow(row, category))
        done()
    })
}

function getCaseCategory(csv_path) {
    let csvType;
    if(csv_path.includes("confirmed"))
        csvType = "confirmed";
    else if(csv_path.includes("deaths"))
        csvType = "deaths"
    else
        csvType = "recovered";
    return csvType
}

function processRow(row, category) {
    const dateColumns = Object.keys(row).filter((k) => /^\d+\/\d+\/\d+/.test(k))
    const dailyCounts = dateColumns.map((date) => {
        return {
            date : date,
            count: parseInt(row[date]) // number of cases/deaths/ ..  for  `date`
        }
    })
    
    dailyCounts.forEach( count => {
        const newEntry = makeEntry(row, count['date']); // makes new entry
        const oldEntry = getEntryIfExists(newEntry, count) // get same entry if exists in the index
        
        // If exists ==> increment in the count on in entry reference
        // else (missing) ==> add entry to indexes
        if( oldEntry ) {
            oldEntry['raw'][category] += count.count
            addGeoInfoToExistingEntry(oldEntry, newEntry.coordinates[0])
        }
        else {
            newEntry['raw'][category] = count.count
            addToDateIndex(newEntry);
            addToCountryIndex(newEntry)
        }
    })
}

function makeEntry(row, date) {
    const country = (row['Country/Region'] || row['Country_Region']).toLowerCase().trim()

    let d = new Date(date)
    const userTimezoneOffset = d.getTimezoneOffset() * 60000;
    return {
        'country': country,
        'coordinates' : [{
            'lat': row["Lat"],
            'lon': row["Long"]
        }],
        'date': new Date(d.getTime() - userTimezoneOffset),
        'raw': {
            'confirmed': 0,
            'deaths': 0,
            'recovered': 0
        }
    };
}

function getEntryIfExists(newEntry, count) {
    if (country_index.has(newEntry.country))
        return country_index.get(newEntry.country).filter(x=>x.date.toDateString() === new Date(count.date).toDateString())[0]

}

function addGeoInfoToExistingEntry(oldEntry, newGeo){
    const hasGeo = oldEntry.coordinates.filter(({ lat, lon }) => {
        return lat === newGeo.lat &&  lon === newGeo.lon
    })
    
    if (hasGeo.length === 0 && notNullGeo(oldEntry, newGeo)) {
        oldEntry.coordinates.push(newGeo)
    }
}

function notNullGeo(oldEntry, newGeo) {
    return !('0.0' === newGeo.lat && '0.0');
}

function addToDateIndex(newEntry) {
    if (date_index.get(newEntry.date.toDateString())) {
        date_index.get(newEntry.date.toDateString()).push(newEntry)
    } else {
        date_index.set(newEntry.date.toDateString(), [newEntry])
    }
}

function addToCountryIndex(newEntry){
    if (country_index.get(newEntry.country)) {
        country_index.get(newEntry.country).push(newEntry)
    } else {
        country_index.set(newEntry.country, [newEntry])
    }
}

const getDateIndex = function(){
    return date_index;
};

const getCountryIndex = function(){
    return country_index;
}

module.exports = {
    'init': init,
    'date_index':getDateIndex,
    'country_index':getCountryIndex,
};
