const loader = require("./loader");

loader.init();

module.exports = {
    country_index:loader.country_index(),
    date_index:loader.date_index()
}
