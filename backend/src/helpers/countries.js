const { findAllDistancesFromCoords } = require("./country");
const { date_index } = require("../store");

const getDailyTotals = (fromCoords, radius) => {
    
    let distances;
    if (fromCoords && radius)
        distances = findAllDistancesFromCoords(fromCoords);
    
    const daily_totals = []

    let daily_data

    date_index.forEach((countries_daily, day) => {

        daily_data = {
            day,
            total: {
                confirmed: 0,
                deaths: 0,
                recovered: 0
            }
        }

        countries_daily.forEach((c) => {

            const { confirmed, deaths, recovered } = c.raw
    
            let shouldAggregate = true
            
            if (fromCoords && radius) {
                const { distance } = distances[c.country]
                if ( radius && distance > radius ) shouldAggregate = false
            }
    
            if (shouldAggregate) {
                daily_data.total.confirmed += confirmed
                daily_data.total.deaths += deaths
                daily_data.total.recovered += recovered
            }

        })
        daily_totals.push(daily_data)
    })

    return daily_totals
}

module.exports = {
    getDailyTotals
}
