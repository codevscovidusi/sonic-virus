const {country_index, date_index} = require("../store");
const countryJs = require('country-js');
const geolib = require('geolib');
const Compass = require("cardinal-direction");

const countryExists = (countryName) => {
    return findByCountry(countryName) !== undefined
}

const getCountriesList = () => {
    const countries = {}

    country_index.forEach((data, country) => {
        const {lat, lon} = data[0]
        countries[country] = {lat, lon}
    })

    return countries
}

const findByCountry = (countryName) => {
    return country_index.get(countryName)
}

const getCountry = (countryName) => {
    return country_index.get(countryName)[0]
}

const findDistanceBetweenString = (fromCountry, toCountry) => {
    let fromCountryGeo = countryJs.search(fromCountry)[0]
    let toCountryGeo = countryJs.search(toCountry)[0]
    if (fromCountryGeo && toCountryGeo)
        return geolib.getDistance(fromCountryGeo['geo'], toCountryGeo['geo']);
}

const findAllDistancesFromCoords = (fromCoords) => {

    const {lat: f_lat, lon: f_lon} = fromCoords

    const from_country_geo = {
        latitude: parseFloat(f_lat),
        longitude: parseFloat(f_lon)
    }

    const distances = {}

    country_index.forEach((data, to_country) => {
        
        const { coordinates } = getCountry(to_country)
        
        const min_dist = getMinDistance(from_country_geo, coordinates)
        
        distances[to_country] = {
            distance: min_dist > 0 ? min_dist / 1000 : 0, // convert from meters to km
        }
    })

    return distances
}

const getMinDistance = (from_country_geo, coordinates) => {
    return coordinates.map((geo) => {
        const to_country_geo = {
            latitude: parseFloat(geo.lat),
            longitude: parseFloat(geo.lon)
        }
    
        return  geolib.getDistance(from_country_geo, to_country_geo)
    }).reduce((min, current) => {
        return Math.min(min, current)
    })
}

const findDirectionBetweenString = (fromCountry, toCountry) => {
    let fromCountryGeo = countryJs.search(fromCountry)[0]['geo'];
    let toCountryGeo = countryJs.search(toCountry)[0]['geo'];
    let compass = geolib.getCompassDirection(
        fromCountryGeo,
        toCountryGeo
    );
    return {cardinal: compass, degrees: Compass.degreeFromCardinal(compass)}
}

const findDistanceBetweenGeoCoordinates = (fromCountryGeo, toCountryGeo) => {
    return geolib.getDistance(
        fromCountryGeo,
        toCountryGeo
    );
}

const getWeightedAggDataFromCoords = (fromCoords, radius) => {

    const distances = findAllDistancesFromCoords(fromCoords);

    const w_agg_data = []
    let max_conf = 0, max_dead = 0, max_rec = 0;
    let aggregate, data
    
    date_index.forEach((countries_daily, day) => {
        
        aggregate = {
            confirmed: 0,
            deaths: 0,
            recovered: 0
        }
        
        data = []
        let total = 0;
        countries_daily.forEach((c) => {
            const { confirmed, deaths, recovered } = c.daily
            
            const { distance } = distances[c.country]
            let shouldAggregate = true
            if ( radius && distance > radius) shouldAggregate = false
            
            if (shouldAggregate) {
                if (confirmed && confirmed >= 0){
                    aggregate.confirmed += (confirmed/Math.max(distance, 1))
                    max_conf = Math.max(aggregate.confirmed, max_conf)
                    total += confirmed
                }
                if (deaths && deaths >= 0) {
                    aggregate.deaths += (deaths/Math.max(distance, 1))
                    max_dead = Math.max(aggregate.deaths, max_dead)
                    total += deaths
                }
                if (recovered && recovered >= 0) {
                    aggregate.recovered += (recovered/Math.max(distance, 1))
                    max_rec = Math.max(aggregate.recovered, max_rec)
                    total += recovered
                }
    
                if( confirmed !== 0 || deaths !== 0 || recovered !== 0 &&
                    (confirmed > 0 && deaths > 0 && recovered > 0))
                    data.push({
                        country: c.country,
                        confirmed,
                        deaths,
                        recovered
                    })
            }
        })
        w_agg_data.push({
            day,
            total,
            aggregate,
            data
        })
    })

    w_agg_data.forEach((day) => {
        const { confirmed, deaths, recovered } = day.aggregate
        if (confirmed > 0) day.aggregate.confirmed = (confirmed/max_conf).toFixed(2)
        if (deaths > 0) day.aggregate.deaths = (deaths/max_dead).toFixed(2)
        if (recovered > 0) day.aggregate.recovered = (recovered/max_rec).toFixed(2)
    })

    return w_agg_data
}

const getGeoFromString = (country) => {
    return countryJs.search(country)[0]
}

module.exports = {
    countryExists,
    getCountriesList,
    findByCountry,
    findDistanceBetweenString,
    findAllDistancesFromCoords,
    findDirectionBetweenString,
    findDistanceBetweenGeoCoordinates,
    getGeoFromString,
    getWeightedAggDataFromCoords
}
