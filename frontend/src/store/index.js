import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import api from '../config/api';

const {
  LOAD_GLOBAL_DATA,
  LOAD_COUNTRY_DATA,
  LOAD_RADIUS_DATA,
  LOAD_COUNTRY,
  REFRESH_COUNTRY,
  PLAY,
  PAUSE,
  STOP,
  CHANGE_CURRENT_DATA_INDEX,
  CHANGE_DATA_REFRESH_INTERVAL,
  CHANGE_DATA_RANGE,
  CHANGE_VOLUME,
  CHANGE_SET_CANVAS_ENABLED_CALLBACK,
  CALL_SET_CANVAS_ENABLED,
  CHANGE_USE_RADIUS,
  CHANGE_RADIUS,
  CHANGE_SET_GLOBE_AUTOROTATION_CALLBACK,
  CALL_SET_GLOBE_AUTOROTATION,
  CHANGE_GLOBE_AUTOROTATION,
  CHANGE_SHOW_TRANSCRIPT,
  CHANGE_SHOW_CHART,
} = actions;

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // contains global data about the deaths by day
    globalData: [],
    // contains country data about the deaths by day
    countryData: [],
    // contains country data about the deaths by day given a range
    radiusData: [],
    // contains lat, lon and countryName
    selectedCountry: undefined,
    // array of data points
    country: [],
    // index of data point that is currently playing
    currentDataIndex: 0,
    // range of indices of data points that are being played on a loop
    dataRange: [],
    // whether audio is playing or not
    playing: false,
    // how often we change the currentDataIndex when playing
    dataRefreshInterval: 2,
    // callback to enable or disable canvas
    setCanvasEnabled: null,
    // whether to use radius or not
    useRadius: false,
    // actual radius value
    radius: Math.log(1000),
    // callback to enable or disable autorotation
    setGlobeAutorotation: null,
    // whether to enable autorotation or not
    globeAutorotation: false,
    // whether to show transcript or not
    showTranscript: true,
    // whether to show chart or not
    showChart: true,
    volume: 1,
    // show if isMobile view
    isMobile: false,
  },
  getters: {
    // the maximum daily number of total cases for the given selection
    maxTotalNumber: (state) => Math.max(...state.country.map((c) => c.total)),
  },
  mutations: {
    SET_GLOBAL_DATA(state, globalData) {
      state.globalData = globalData;
    },
    SET_COUNTRY_DATA(state, countryData) {
      state.countryData = countryData;
    },
    SET_RADIUS_DATA(state, rangeData) {
      state.radiusData = rangeData;
    },
    SET_COUNTRY(state, country) {
      state.country = country;
    },
    SET_SELECTED_COUNTRY(state, selectedCountry) {
      state.selectedCountry = selectedCountry;
    },
    SET_PLAYING(state, playing) {
      state.playing = playing;
    },
    SET_CURRENT_DATA_INDEX(state, idx) {
      state.currentDataIndex = idx;
    },
    SET_DATA_REFRESH_INTERVAL(state, value) {
      state.dataRefreshInterval = value;
    },
    SET_DATA_RANGE(state, range) {
      state.dataRange = range;
    },
    SET_SET_CANVAS_ENABLED_CALLBACK(state, callback) {
      state.setCanvasEnabled = callback;
    },
    SET_VOLUME(state, val) {
      state.volume = val;
    },
    SET_USE_RADIUS(state, val) {
      state.useRadius = val;
      if (val === false) {
        state.radius = Math.log(1000);
      }
    },
    SET_RADIUS(state, val) {
      state.radius = val;
    },
    SET_SET_GLOBE_AUTOROTATION_CALLBACK(state, callback) {
      state.setGlobeAutorotation = callback;
    },
    SET_GLOBE_AUTOROTATION(state, value) {
      state.globeAutorotation = value;
    },
    SET_SHOW_TRANSCRIPT(state, value) {
      state.showTranscript = value;
    },
    SET_SHOW_CHART(state, value) {
      state.showChart = value;
    },
    SET_MOBILE(state, value) {
      state.isMobile = value;
    },
  },
  actions: {
    [LOAD_GLOBAL_DATA]({ commit }) {
      return Vue.axios
        .get(api.routes.globalData()).then((result) => {
          commit('SET_GLOBAL_DATA', result.data.daily_totals);
        });
    },
    [LOAD_COUNTRY_DATA]({ commit }, name) {
      return Vue.axios
        .get(api.routes.countryData(name)).then((result) => {
          commit('SET_COUNTRY_DATA', result.data);
        });
    },
    [LOAD_RADIUS_DATA]({ commit }, { lat, lon, radius }) {
      return Vue.axios
        .get(api.routes.radiusData(lat, lon, Math.exp(radius))).then((result) => {
          commit('SET_RADIUS_DATA', result.data);
        });
    },
    [LOAD_COUNTRY]({ commit }, { lat, lon, countryName }) {
      return Vue.axios
        .get(this.state.useRadius
          ? api.routes.countryWithRadius({ lat, lon }, Math.exp(this.state.radius))
          : api.routes.country({ lat, lon }))
        .then((result) => {
          const { data: country } = result.data;
          commit('SET_COUNTRY', country);
          if (this.state.dataRange.length === 0) {
            commit('SET_DATA_RANGE', [0, country.length - 1]);
          }
          commit('SET_SELECTED_COUNTRY', { lat, lon, countryName });
          commit('SET_PLAYING', true);
        }).catch((error) => {
          throw new Error(`API ${error}`);
        });
    },
    [REFRESH_COUNTRY]({ dispatch, state }) {
      dispatch(LOAD_COUNTRY, state.selectedCountry);
    },
    [PLAY]({ commit }) {
      commit('SET_PLAYING', true);
    },
    [PAUSE]({ commit }) {
      commit('SET_PLAYING', false);
    },
    [STOP]({ commit }) {
      commit('SET_PLAYING', false);
      commit('SET_CURRENT_DATA_INDEX', 0);
    },
    [CHANGE_CURRENT_DATA_INDEX]({ commit }, index) {
      commit('SET_CURRENT_DATA_INDEX', index);
    },
    [CHANGE_DATA_REFRESH_INTERVAL]({ commit }, value) {
      commit('SET_DATA_REFRESH_INTERVAL', value);
    },
    [CHANGE_DATA_RANGE]({ commit, state }, range) {
      commit('SET_DATA_RANGE', range);
      const { currentDataIndex: idx } = state;
      const [min, max] = range;
      // Set current data index to a legal value (inside the range)
      commit('SET_CURRENT_DATA_INDEX', Math.min(Math.max(min, idx), max));
    },
    [CHANGE_SET_CANVAS_ENABLED_CALLBACK]({ commit }, callback) {
      commit('SET_SET_CANVAS_ENABLED_CALLBACK', callback);
    },
    [CALL_SET_CANVAS_ENABLED]({ state }, flag) {
      state.setCanvasEnabled(flag);
    },
    [CHANGE_USE_RADIUS]({ commit }, value) {
      commit('SET_USE_RADIUS', value);
    },
    [CHANGE_RADIUS]({ commit }, value) {
      commit('SET_RADIUS', value);
    },
    [CHANGE_SET_GLOBE_AUTOROTATION_CALLBACK]({ commit }, callback) {
      commit('SET_SET_GLOBE_AUTOROTATION_CALLBACK', callback);
    },
    [CALL_SET_GLOBE_AUTOROTATION]({ state }, flag) {
      state.setGlobeAutorotation(flag);
    },
    [CHANGE_GLOBE_AUTOROTATION]({ commit }, value) {
      commit('SET_GLOBE_AUTOROTATION', value);
    },
    [CHANGE_SHOW_TRANSCRIPT]({ commit }, value) {
      commit('SET_SHOW_TRANSCRIPT', value);
    },
    [CHANGE_SHOW_CHART]({ commit }, value) {
      commit('SET_SHOW_CHART', value);
    },
    [CHANGE_VOLUME]({ commit }, val) {
      commit('SET_VOLUME', val);
    },
  },
  modules: {
  },
});
