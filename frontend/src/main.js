import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueApexCharts from 'vue-apexcharts';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import App from '@/App.vue';
import store from '@/store';
import api from '@/config/api';
import router from './router';
import i18n from './i18n';

library.add(fas);
library.add(far);
library.add(fab);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('apexchart', VueApexCharts);
Vue.use(Buefy);
Vue.use(VueAxios, axios);
Vue.config.productionTip = false;

Vue.axios.defaults.baseURL = api.baseUrl;

new Vue({
  store,
  router,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
