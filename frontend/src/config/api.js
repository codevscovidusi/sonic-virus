import env from '@/config/env';

const baseUrl = env.isProduction ? 'https://sonicvirus.si.usi.ch/api' : 'http://localhost:3000';

const routes = {
  country: (coords) => `/country/${coords.lon}/${coords.lat}/aggregated`,
  countryWithRadius: (coords, radius) => `/country/${coords.lon}/${coords.lat}/aggregated/radius/${radius}`,
  globalData: () => '/countries/daily-totals',
  radiusData: (lat, lon, radius) => `countries/${lon}/${lat}/daily-totals/${radius}`,
  countryData: (name) => `/country/${name}`,
};

export default {
  baseUrl,
  routes,
};
