export default {
  'en-US': {
    short: {
      year: 'numeric', month: 'short', day: 'numeric',
    },
  },
  'ja-JP': {
    short: {
      year: 'numeric', month: 'short', day: 'numeric',
    },
  },
};
