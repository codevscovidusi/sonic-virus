import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from './locales/en.json';
import it from './locales/it.json';
import dateFormat from './formatter/date-format';

Vue.use(VueI18n);

export default new VueI18n({
  locale: ((navigator.languages && navigator.languages[0]) || '').substr(0, 2),
  fallbackLocale: 'en',
  dateFormat,
  messages: {
    en, it,
  },
});
