import About from '../../components/pages/shared/About.vue';

export default
{
  path: '/about',
  name: 'about',
  components: {
    default: About,
    mobile: About,
  },
};
