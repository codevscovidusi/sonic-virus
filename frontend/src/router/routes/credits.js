import Credits from '../../components/pages/shared/Credits.vue';

export default
{
  path: '/credits',
  name: 'credits',
  components: {
    default: Credits,
    mobile: Credits,
  },
};
