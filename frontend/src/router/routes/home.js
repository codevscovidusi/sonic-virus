import HomeDesktop from '../../components/pages/desktop/HomeDesktop.vue';
import HomeMobile from '../../components/pages/mobile/HomeMobile.vue';

export default {
  path: '/',
  name: 'Home',
  components: {
    default: HomeDesktop,
    mobile: HomeMobile,
  },
};
