import Vue from 'vue';
import Router from 'vue-router';
import visualisation from './routes/home';
import about from './routes/about';
import credits from './routes/credits';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'is-active',
  routes: [visualisation, about, credits],
});
