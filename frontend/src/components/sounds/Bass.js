import SoundDevice from './SoundDevice';
import Compressor from './Compressor';
import Reverb from './Reverb';

export default class Bass extends SoundDevice {
  constructor(context, master, fundFrequency) {
    super(context, master, 0.015);

    this.playing = false;

    this.fundFreq = fundFrequency;
    this.bassFreq = this.bassFreqCalculation();

    // Setup external components
    this.compressor = new Compressor(this.context).get();
    // this.reverb = new Reverb(this.context, '/sound/Natatorium.wav').get();
    // this.reverb = new Reverb(this.context, '/sound/TheSlot.wav').get();
    this.reverb = new Reverb(this.context, '/sound/PortTownsendSkatepark.wav').get();

    // Setup bass components
    this.bassGain = this.context.createGain();
    this.bassGain.gain.value = 0;

    this.bassSource = this.context.createOscillator();
    this.bassSource.type = 'sawtooth';
    this.bassSource.frequency.value = this.bassFreq;
    this.bassSource.start();

    this.bassFilter = this.context.createBiquadFilter();
    this.bassFilter.type = 'lowpass';
    this.bassFilter.frequency.value = this.fundFreq;

    // Fade In
    this.bassGain.gain.cancelScheduledValues(this.context.currentTime);
    this.bassGain.gain.setValueAtTime(0, this.context.currentTime);
    this.bassGain.gain.linearRampToValueAtTime(this.finalVolume, this.context.currentTime + 24);

    // Connections
    this.bassSource.connect(this.bassGain);
    this.bassGain.connect(this.bassFilter);
    this.bassFilter.connect(this.reverb);
    this.reverb.connect(this.compressor);
    this.compressor.connect(this.master);
  }

  bassFreqCalculation() {
    // divide by 2 the fundFreq using 40Hz as low threshold
    let bassFreqCalc = this.fundFreq;
    while (bassFreqCalc > 40) {
      bassFreqCalc /= 2;
    }
    return bassFreqCalc * 2;
  }

  setFundFreq(newFrequency) {
    this.fundFreq = newFrequency;
    this.bassFreq = this.bassFreqCalculation();
    this.bassSource.frequency.value = this.bassFreq; // TODO: rampina
  }
}
