import SoundDevice from './SoundDevice';
import random from '../../utils/random';
import Compressor from './Compressor';
import Reverb from './Reverb';

export default class Noise extends SoundDevice {
  constructor(context, master) {
    super(context, master, 0.05);

    this.playing = false;

    this.compressor = new Compressor(this.context).get();

    // Setup external components
    this.reverb = new Reverb(this.context, '/sound/JFKUnderpass.wav').get();

    // Noise components
    this.expFadeIndGain = 1.6;
    this.noiseGain = context.createGain();
    this.noisePan = context.createStereoPanner();
    this.noiseFilter = context.createBiquadFilter();
    this.noiseFilter.type = 'highpass';
  }

  setPlaying(playing) {
    this.playing = playing;
  }

  updateFadeInGainFrom(data) {
    const maxClamp = 0.1;
    const clampDeaths = Math.min(Math.max(data.deaths, 0), maxClamp);
    const powScale = SoundDevice.expScale(this.expFadeIndGain, 0, maxClamp, 0, 1);
    const newFadeInGainValue = powScale(clampDeaths);
    this.fadeInGain.gain.value = newFadeInGainValue;
  }

  play() {
    if (!this.playing) return;

    const noiseSource = this.context.createBufferSource();

    // Random length of the noise (100ms - 1.1 s)
    const noiseDuration = Math.random() * 0.5 + 0.1;

    // Create an empty mono buffer at the sample rate of the context
    const noiseBuffer = this.context.createBuffer(
      1,
      this.context.sampleRate * noiseDuration,
      this.context.sampleRate,
    );

    // Fill the buffer with white noise (a random number between -1 and 1)
    for (let i = 0; i < noiseBuffer.length; i += 1) {
      noiseBuffer.getChannelData(0)[i] = Math.random() * 2 - 1;
    }

    // Add buffer to source and start it
    noiseSource.buffer = noiseBuffer;
    noiseSource.start();

    // Create Envelope
    const numberOfStepNoiseEnvelope = Math.random() * 4 + 5;
    const stepNoiseEnvelopeDuration = noiseDuration / numberOfStepNoiseEnvelope;
    this.noiseGain.gain.cancelScheduledValues(this.context.currentTime);
    this.noiseGain.gain.setValueAtTime(0, this.context.currentTime);
    for (let i = 0; i < numberOfStepNoiseEnvelope; i += 1) {
      this.noiseGain.gain.linearRampToValueAtTime(
        Math.random() * this.finalVolume,
        this.context.currentTime + (i + 1) * stepNoiseEnvelopeDuration,
      );
    }

    // Create Panning
    const noisePanPosition = random.getRandomBetween(0.8, 1);
    const noisePanDirection = Math.random() > 0.5 ? 1 : -1;
    this.noisePan.pan.setValueAtTime(
      noisePanPosition * noisePanDirection,
      this.context.currentTime,
    );

    // Create Filter
    this.noiseFilter.frequency.setValueAtTime(
      Math.random() * 10000 + 500,
      this.context.currentTime,
    );

    // Connections
    noiseSource.connect(this.fadeInGain);
    this.fadeInGain.connect(this.noiseFilter);
    this.noiseFilter.connect(this.noiseGain);
    this.noiseGain.connect(this.reverb);
    this.reverb.connect(this.compressor);
    this.compressor.connect(this.master);

    noiseSource.connect(this.fadeInGain);
    this.fadeInGain.connect(this.noiseFilter);
    this.noiseFilter.connect(this.noiseGain);
    this.noiseGain.connect(this.noisePan);
    this.noisePan.connect(this.master);
  }
}
