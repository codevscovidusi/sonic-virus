import random from '../../utils/random';
import SoundDevice from './SoundDevice';
import Compressor from './Compressor';
import Reverb from './Reverb';

export default class Arpeggiator extends SoundDevice {
  constructor(context, harmony, master, getMetroArpeggiatorTime) {
    super(context, master, 0.1);

    this.harmony = harmony;

    this.expFadeIndGain = 2;

    this.numberOfNotes = 4;

    this.panLFOMinFreq = 0.1;
    this.panLFOMaxFreq = 0.7;

    this.filterMinFreq = 500;
    this.filterMaxFreq = 2000;

    // JFKUnderpass.wav
    // MillsGreekTheater.wav
    // Natatorium.wav
    // PortTownsendSkatepark.wav
    // TheSlot.wav

    // Setup external components
    this.compressor = new Compressor(this.context).get();
    this.reverb = new Reverb(this.context, '/sound/MillsGreekTheater.wav').get();

    this.getMetroArpeggiatorTime = getMetroArpeggiatorTime;

    this.currentArpFreq = 0;
    this.arpeggiatorStep = -1;

    this.oscillator = this.context.createOscillator();
    this.oscillator.type = 'triangle';
    this.oscillator.frequency.value = this.currentArpFreq;

    // arpeggiator Gain
    this.oscillatorGain = this.context.createGain();
    this.oscillatorGain.gain.value = 0;
    this.oscillatorGain.gain.cancelScheduledValues(0);
    this.oscillatorGain.gain.setValueAtTime(0, this.context.currentTime);

    // arpeggiator Panning
    this.pan = this.context.createStereoPanner();

    this.panLFO = this.context.createOscillator();
    this.panLFO.type = 'sine';
    this.panLFO.frequency.value = random.getRandomBetween(0.05, 0.5);

    this.panLFOGain = this.context.createGain();
    this.panLFOGain.gain.cancelScheduledValues(0);
    this.panLFOGain.gain.setValueAtTime(1, this.context.currentTime);

    this.filter = this.context.createBiquadFilter();
    this.filter.type = 'lowpass';
    this.filter.frequency.value = 2000;

    // Delay
    this.delay = context.createDelay();
    this.feedbackDelay = context.createGain();
    this.delayPanLFO = context.createOscillator();
    this.panDelay = context.createStereoPanner();

    this.delayPanLFO.type = 'sine';
    this.delayPanLFO.start();

    this.feedbackDelayGain = {
      min: 0.3,
      max: 0.7,
    };
    this.feedbackDelayTime = {
      min: 0.05,
      max: 0.5,
    };

    // feedback
    this.delay.connect(this.feedbackDelay);
    this.feedbackDelay.connect(this.delay);

    // apply LFO to Delay Panning
    this.delayPanLFO.frequency.value = random.getRandomBetween(0.1, 1);
    this.delayPanLFO.connect(this.panDelay.pan);

    // pan
    this.panLFO.connect(this.pan.pan);

    this.delay.connect(this.feedbackDelay);
    this.feedbackDelay.connect(this.delay);

    this.oscillator.connect(this.fadeInGain);
    this.fadeInGain.connect(this.oscillatorGain);
    this.oscillatorGain.connect(this.filter);
    this.filter.connect(this.pan);
    this.pan.connect(this.reverb);
    this.reverb.connect(this.compressor);
    this.compressor.connect(this.master);

    // start oscillators
    this.oscillator.start(0);
    this.panLFO.start(0);
  }

  incMetroIndex() {
    const newMetroIndex = (this.harmony.metroIndex + 1) % this.harmony.maxNoPitches;
    this.harmony.metroIndex = newMetroIndex;
  }

  setHarmony(newHarmony) {
    this.harmony = newHarmony;
  }

  getHarmonyFrequencies() {
    const result = [];

    this.harmony.pitches.forEach((pitch) => {
      if (pitch !== undefined) {
        result.push(pitch.currentCarrierFreq);
      }
    });
    return result;
  }

  updateFadeInGainFrom(data) {
    const maxClamp = 0.05;
    const clampConfirmed = Math.min(Math.max(data.confirmed, 0), maxClamp);
    const powScale = SoundDevice.expScale(this.expFadeIndGain, 0, maxClamp, 0, 1);
    const newFadeInGainValue = powScale(clampConfirmed);
    this.fadeInGain.gain.value = newFadeInGainValue;
  }

  play() {
    this.delay.delayTime.value = random.getRandomBetween(
      this.feedbackDelayTime.min,
      this.feedbackDelayTime.max,
    );

    this.oscillatorGain.gain.setValueAtTime(0, this.context.currentTime);

    // set a threshold befor delirium
    const internalMetroTime = Math.max(this.getMetroArpeggiatorTime() / this.numberOfNotes, 0.1);

    const rampDuration = internalMetroTime * 0.15;
    const zeroDuration = internalMetroTime * 0.85;

    this.oscillatorGain.gain.cancelScheduledValues(this.context.currentTime);
    this.oscillatorGain.gain.setValueAtTime(0, this.context.currentTime);

    this.delayPanLFO.frequency.value = random.getRandomBetween(
      this.panLFOMinFreq,
      this.panLFOMaxFreq,
    );

    this.filter.frequency.value = random.getRandomBetween(this.filterMinFreq, this.filterMaxFreq);

    for (let i = 0; i < this.numberOfNotes; i += 1) {
      const currentStart = this.context.currentTime + i * internalMetroTime;

      const freqMultiplier = Arpeggiator.computeFrequencyMultiplier(i);

      this.oscillator.frequency.setValueAtTime(this.currentArpFreq * freqMultiplier, currentStart);
      this.oscillatorGain.gain.linearRampToValueAtTime(this.finalVolume, currentStart);
      this.oscillatorGain.gain.linearRampToValueAtTime(0, currentStart + rampDuration);
      this.oscillatorGain.gain.setValueAtTime(0, currentStart + rampDuration + zeroDuration);
    }
  }

  static computeFrequencyMultiplier(i) {
    let fm = 1;

    if (i === 0) {
      fm = 1;
    } else if (i % 2 === 0) {
      fm = i;
    } else {
      fm = 1.5;
    }

    return fm;
  }

  update() {
    const pitchesFrequencies = this.getHarmonyFrequencies();

    if (pitchesFrequencies.length === 0) return;

    const randomFreqIndex = Math.floor(random.getRandomBetween(0, pitchesFrequencies.length - 1));
    this.currentArpFreq = pitchesFrequencies[randomFreqIndex];
    this.oscillator.frequency.cancelScheduledValues(0);
    this.oscillator.frequency.setValueAtTime(this.currentArpFreq, this.context.currentTime);
  }
}
