import random from '../../utils/random';
import SoundDevice from './SoundDevice';
import Compressor from './Compressor';
import Reverb from './Reverb';

export default class Pitch extends SoundDevice {
  constructor(context, getDataRefreshTime, harmony, master) {
    super(context, master, 0.0003);

    this.compressor = new Compressor(this.context).get();

    // this.reverb = new Reverb(this.context, '/sound/TheSlot.wav').get();
    this.reverb = new Reverb(this.context, '/sound/JFKUnderpass.wav').get();

    // Setup external components
    this.waveShaper = this.context.createWaveShaper();
    this.waveShaper.curve = Pitch.makeDistortionCurve(400);

    this.getDataRefreshTime = getDataRefreshTime;
    this.harmony = harmony;
    this.maxHarmonicDistortionDelta = 3;

    this.currentCarrierFreq = 0;
    this.currentModFreq = 0;

    this.oscCarrier = this.context.createOscillator();
    this.oscCarrier.type = 'triangle';

    this.oscModulator = this.context.createOscillator();
    this.oscModulator.type = 'triangle';

    this.carrierGain = this.context.createGain();
    this.updateCarrierGain();

    this.modulatorGain = this.context.createGain();
    this.modulatorGain.gain.value = 0;
    this.modulatorGain.gain.cancelScheduledValues(0);
    this.modulatorGain.gain.setValueAtTime(0, this.context.currentTime);

    this.updateOscFrequency();
    this.updateModFrequency();

    this.pan = this.context.createStereoPanner();
    this.panLFO = this.context.createOscillator();

    this.panLFO.type = 'sine';
    this.panLFO.frequency.value = random.getRandomBetween(0.02, 0.1);

    // connections
    this.panLFO.connect(this.pan.pan);
    this.oscModulator.connect(this.modulatorGain);
    this.modulatorGain.connect(this.carrierGain.gain);

    this.oscCarrier.connect(this.carrierGain);
    this.carrierGain.connect(this.waveShaper);
    this.waveShaper.connect(this.pan);
    this.pan.connect(this.compressor);
    this.compressor.connect(this.reverb);
    this.reverb.connect(this.master);

    // start oscillators
    this.oscCarrier.start(0);
    this.oscModulator.start(0);
    this.panLFO.start(0);
  }

  static makeDistortionCurve(amount) {
    const k = typeof amount === 'number' ? amount : 50;
    const nSamples = 44100;
    const curve = new Float32Array(nSamples);
    const deg = Math.PI / 180;
    let x;
    let i;
    for (i = 0; i < nSamples; i += 1) {
      x = (i * 2) / nSamples - 1;
      curve[i] = ((3 + k) * x * 20 * deg) / (Math.PI + k * Math.abs(x));
    }
    return curve;
  }

  updateOscFrequency() {
    this.oscCarrier.frequency.value = this.currentCarrierFreq;
    this.oscCarrier.frequency.cancelScheduledValues(0);
    this.oscCarrier.frequency.setValueAtTime(this.currentCarrierFreq, this.context.currentTime);

    // this is the final volume of the triangle harm field pitches
    this.modulatorGain.gain.setValueAtTime(0, this.context.currentTime);

    this.modulatorGain.gain.linearRampToValueAtTime(
      this.finalVolume,
      this.context.currentTime + this.getDataRefreshTime() * (this.harmony.maxNoPitches / 3),
    );
    this.modulatorGain.gain.linearRampToValueAtTime(
      this.finalVolume,
      this.context.currentTime + this.getDataRefreshTime() * (this.harmony.maxNoPitches - 1),
    );
    this.modulatorGain.gain.linearRampToValueAtTime(
      0,
      this.context.currentTime + this.getDataRefreshTime() * this.harmony.maxNoPitches,
    );
  }

  updateModFrequency() {
    this.oscModulator.frequency.value = this.currentModFreq;
    this.oscModulator.frequency.cancelScheduledValues(0);
    this.oscModulator.frequency.setValueAtTime(this.currentModFreq, this.context.currentTime);
  }

  updateCarrierGain() {
    const currentCarrierGain = 0;

    this.carrierGain.gain.value = currentCarrierGain;
    this.carrierGain.gain.cancelScheduledValues(0);
    this.carrierGain.gain.setValueAtTime(currentCarrierGain, this.context.currentTime);
  }

  updateCarrierFrequencyFrom(data) {
    const delta = SoundDevice.scaleLinear(0, 1, 0, this.maxHarmonicDistortionDelta, data);
    // This is magic! Don't ask.
    const fundHarmFieldIdx = Math.floor(
      random.getRandomBetween(0, this.harmony.fundHarmField.length - 1),
    );
    const deltaHarmony = this.harmony.fundHarmField[fundHarmFieldIdx] + delta;
    // distorsion of the field
    this.currentCarrierFreq = this.harmony.fundFreq * deltaHarmony;
    this.updateOscFrequency();
  }

  updateModulatorFrequencyFrom(data) {
    // Updating the modulator freq for amplitude envelope
    const scale = SoundDevice.expScale(0.6, 0, 1, 0.01, 4);
    this.currentModFreq = scale(data);
    this.updateModFrequency();
  }

  updateFrequenciesFrom(data) {
    this.updateCarrierFrequencyFrom(data);
    this.updateModulatorFrequencyFrom(data);
  }
}
