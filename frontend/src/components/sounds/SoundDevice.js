import * as d3 from 'd3';

export default class SoundDevice {
  constructor(context, master, finalVolume) {
    this.context = context;
    this.master = master;
    this.finalVolume = finalVolume;

    this.fadeInGain = context.createGain();
    this.fadeInGain.gain.value = 0;
  }

  /* eslint-disable */
  updateFadeInGainFrom(data) {
    // Subclass Responsibility
  }
  /* eslint-enable */

  static scaleLinear(domainMin, domainMax, rangeMin, rangeMax, value) {
    const scale = d3.scaleLinear().domain([domainMin, domainMax]).range([rangeMin, rangeMax]);
    return scale(value);
  }

  static expScale(exp, domainMin, domainMax, rangeMin, rangeMax) {
    return d3.scalePow().exponent(exp).domain([domainMin, domainMax]).range([rangeMin, rangeMax]);
  }
}
