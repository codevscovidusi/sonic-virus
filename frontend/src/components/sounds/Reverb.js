export default class Reverb {
  constructor(context, impulseResponsePath) {
    // Setup external components
    this.context = context;

    // https://web-audio-api.firebaseapp.com/convolver-node
    this.impulseResponse = undefined;
    this.convolverNode = this.context.createConvolver();
    fetch(impulseResponsePath)
      .then((response) => response.arrayBuffer())
      .then((buffer) => {
        this.context
          .decodeAudioData(buffer, (decoded) => {
            this.convolverNode.buffer = decoded;
            this.impulseResponse = decoded;
          })
          .catch(console.log);
      })
      .catch(console.log);
  }

  get() {
    return this.convolverNode;
  }
}
