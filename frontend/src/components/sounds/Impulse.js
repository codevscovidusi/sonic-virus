import random from '../../utils/random';
import SoundDevice from './SoundDevice';
import Compressor from './Compressor';

export default class Impulse extends SoundDevice {
  constructor(context, master) {
    super(context, master, 0.35);

    this.playing = false;

    // Setup external components
    this.compressor = new Compressor(this.context).get();

    this.feedbackDelayGain = {
      min: 0.3,
      max: 0.7,
    };
    this.feedbackDelayTime = {
      min: 0.05,
      max: 0.5,
    };

    this.expFadeIndGain = 1.6;
    this.impulseGain = context.createGain();
    this.impulsePan = context.createStereoPanner();
    this.impulsePanDelay = context.createStereoPanner();
    this.impulseDelay = context.createDelay();
    this.impulseFeedbackDelay = context.createGain();
    this.impulseDelayPanLFO = context.createOscillator();
    this.impulseDelayPanLFO.type = 'sine';
    this.impulseDelayPanLFO.start();
  }

  setPlaying(playing) {
    this.playing = playing;
  }

  updateFadeInGainFrom(data) {
    const maxClamp = 0.05;
    const clampConfirmed = Math.min(Math.max(data.confirmed, 0), maxClamp);
    const powScale = SoundDevice.expScale(this.expFadeIndGain, 0, maxClamp, 0, 1);
    const newFadeInGainValue = powScale(clampConfirmed);
    this.fadeInGain.gain.value = newFadeInGainValue;
  }

  play() {
    if (!this.playing) return;

    this.impulseDelay.delayTime.value = random.getRandomBetween(
      this.feedbackDelayTime.min,
      this.feedbackDelayTime.max,
    );
    this.impulseFeedbackDelay.gain.value = random.getRandomBetween(
      this.feedbackDelayGain.min,
      this.feedbackDelayGain.max,
    );

    this.impulseDelay.connect(this.impulseFeedbackDelay);
    this.impulseFeedbackDelay.connect(this.impulseDelay);

    const impulseSource = this.context.createBufferSource();
    const impulseDuration = 0.01; // duration of the impulse
    const impulseBuffer = this.context.createBuffer(
      1,
      this.context.sampleRate * impulseDuration,
      this.context.sampleRate,
    );

    for (let i = 0; i < impulseBuffer.length; i += 1) {
      impulseBuffer.getChannelData(0)[i] = this.finalVolume;
    }
    impulseSource.buffer = impulseBuffer;
    impulseSource.start();

    this.impulseGain.gain.cancelScheduledValues(this.context.currentTime);
    this.impulseGain.gain.setValueAtTime(0.3, this.context.currentTime);

    // Create Panning
    const impulsePanPosition = random.getRandomBetween(0.2, 0.8);
    const impulsePanDirection = Math.random() > 0.5 ? 1 : -1;
    this.impulsePan.pan.setValueAtTime(
      impulsePanPosition * impulsePanDirection,
      this.context.currentTime,
    );

    this.impulseDelayPanLFO.frequency.value = random.getRandomBetween(0.1, 4);

    // Connections
    this.impulseDelayPanLFO.connect(this.impulsePanDelay.pan); // apply LFO to Delay Panning

    // Delay
    impulseSource.connect(this.fadeInGain);
    this.fadeInGain.connect(this.impulseDelay);
    this.impulseDelay.connect(this.impulsePanDelay);
    this.impulsePanDelay.connect(this.master);

    // Impulse
    impulseSource.connect(this.fadeInGain);
    this.fadeInGain.connect(this.impulseGain);
    this.impulseGain.connect(this.impulsePan);
    this.impulsePan.connect(this.master);
  }
}
