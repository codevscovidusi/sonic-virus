import Arpeggiator from './Arpeggiator';

export default class TwinArpeggiator extends Arpeggiator {
  constructor(context, harmony, master, arpeggiator) {
    super(context, harmony, master, arpeggiator.getMetroArpeggiatorTime);

    this.masterArpeggiator = arpeggiator;

    this.finalVolume = 0.05;

    this.numberOfNotes = 8;

    this.filterMinFreq = 2000;
    this.filterMaxFreq = 10000;

    this.panLFOMinFreq = 1;
    this.panLFOMaxFreq = 1;

    this.pan.disconnect(this.reverb);
    this.pan.connect(this.compressor);
  }
}
