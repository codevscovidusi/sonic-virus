export default class Compressor {
  constructor(context) {
    // Setup external components
    this.context = context;

    this.compressor = context.createDynamicsCompressor();
    this.compressor.threshold.setValueAtTime(-40, this.context.currentTime);
    this.compressor.knee.setValueAtTime(40, this.context.currentTime);
    this.compressor.ratio.setValueAtTime(12, this.context.currentTime);
    this.compressor.attack.setValueAtTime(0, this.context.currentTime);
    this.compressor.release.setValueAtTime(0.25, this.context.currentTime);
  }

  get() {
    return this.compressor;
  }
}
