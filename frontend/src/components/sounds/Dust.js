import SoundDevice from './SoundDevice';
import Reverb from './Reverb';

export default class Dust extends SoundDevice {
  constructor(context, master) {
    super(context, master, 0.27);

    this.dustPath = '/sound/Dust.mp3';
    this.reverb = new Reverb(this.context, '/sound/Natatorium.wav').get();
    this.dustFadeInGain = context.createGain();
    this.currentGain = context.createGain();

    fetch(this.dustPath)
      .then((response) => response.arrayBuffer())
      .then((arrayBuffer) => this.context.decodeAudioData(arrayBuffer))
      .then((audioBuffer) => {
        this.source = this.context.createBufferSource();
        this.source.buffer = audioBuffer;
        this.source.loop = true;

        this.dustFadeInGain.gain.cancelScheduledValues(0);
        this.dustFadeInGain.gain.setValueAtTime(0, this.context.currentTime);
        this.dustFadeInGain.gain.linearRampToValueAtTime(1.0, this.context.currentTime + 30);

        this.currentGain.gain.cancelScheduledValues(0);
        this.currentGain.gain.setValueAtTime(0, this.context.currentTime);

        // Connections
        this.source
          .connect(this.dustFadeInGain)
          .connect(this.currentGain)
          .connect(this.reverb)
          .connect(this.master);

        this.source.start();
      });
  }

  updateGainFrom(data) {
    const newGain = SoundDevice.scaleLinear(0, 1, 0.02, this.finalVolume, data.confirmed);
    this.currentGain.gain.cancelScheduledValues(0);
    this.currentGain.gain.linearRampToValueAtTime(newGain, this.context.currentTime + 0.5);
  }
}
