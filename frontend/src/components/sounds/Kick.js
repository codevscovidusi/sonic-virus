import SoundDevice from './SoundDevice';
import Compressor from './Compressor';

export default class Kick extends SoundDevice {
  constructor(context, master) {
    super(context, master, 0.5);

    this.playing = false;

    this.iteration = 0;

    // Setup external components
    this.compressor = new Compressor(this.context).get();

    // Setupd kick components
    this.kickGain = this.context.createGain();
    this.kickGain.gain.value = 0;
    this.kickGain.gain.cancelScheduledValues(this.context.currentTime);
    this.kickGain.gain.setValueAtTime(0, this.context.currentTime);
    this.kickSource = this.context.createOscillator();
    this.kickSource.type = 'sawtooth';
    this.kickSource.frequency.value = 30;
    this.kickSource.start();

    this.kickFilter = this.context.createBiquadFilter();
    this.kickFilter.type = 'lowpass';
    this.kickFilter.frequency.value = 80;
  }

  setPlaying(playing) {
    this.playing = playing;
  }

  /* eslint-disable */
  updateFadeInGainFrom(data) {
    const maxClamp = 5;
    const clampIteration = Math.min(Math.max(this.iteration, 0), maxClamp);
    const powScale = SoundDevice.expScale(3, 0, maxClamp, 0, 1);
    const newFadeInGainValue = powScale(clampIteration);
    this.fadeInGain.gain.value = newFadeInGainValue;
  }
  /* eslint-enable */

  play() {
    if (!this.playing) return;

    this.iteration += 1;

    const kickDuration = 0.1;

    // Create Envelope
    this.kickGain.gain.cancelScheduledValues(this.context.currentTime);
    this.kickGain.gain.linearRampToValueAtTime(this.finalVolume, this.context.currentTime);
    this.kickGain.gain.linearRampToValueAtTime(0, this.context.currentTime + kickDuration);

    // Connections
    this.kickSource.connect(this.fadeInGain);
    this.fadeInGain.connect(this.kickFilter);
    this.kickFilter.connect(this.kickGain);
    this.kickGain.connect(this.compressor);
    this.compressor.connect(this.master);
  }
}
