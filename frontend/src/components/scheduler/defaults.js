/**
 * This file contains many magic numbers.
 */

import SoundDevice from '../sounds/SoundDevice';

/**
 * Default scheduling times used when initializing the scheduling
 * of sound playing for the respective sounds.
 */
const getMetroTimes = () => ({
  kick: 2,
  noise: 4,
  impulse: 3,
  arpeggiator: 0.2,
});

// Linking the Fundamental Frequency (200-500Hz) to the selected longitude (±180)
const getHarmony = (longitude) => ({
  fundFreq: SoundDevice.scaleLinear(-180, 180, 200, 400, longitude),
  fundHarmField: [1, 0.5, 1.5, 2, 1.75, 0.25, 1.25, 0.75],
  pitches: [],
  maxNoPitches: 6,
  metroIndex: -1,
});

export default {
  getMetroTimes,
  getHarmony,
};
