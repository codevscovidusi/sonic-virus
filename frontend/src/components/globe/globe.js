import * as Three from 'three';
// import * as TrackballControls from 'three-trackballcontrols';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { Easing, Tween, autoPlay } from 'es6-tween';
import { Object3D } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { geoContains } from 'd3-geo';
import store from '../../store';
import actions from '../../store/actions';
import countryTopojson from '../../assets/countries-50m.json';
import nameResolver from './countryNameResolver';


export default class Globe {
  enabled = true;

  EARTH_RADIUS = 10;

  // ball configuration

  COLOR_DEAD = 0xfa5b5a;

  COLOR_CONFIRMED = 0x23a3c0;

  COLOR_RECOVERED = 0x8cff57;

  COLOR_RADIUS = 0xffcb6b;

  // checking where to display spheres 70% of the render frames
  VISUALISATION_SAMPLING_FREQUENCY = 0.4;

  animationDuration= 2000;

  countryToPoly= null;

  rayCaster= null;

  scene= null;

  renderer= null;

  canvas= null;

  planet= null;

  camera= null;

  controlsOrbit= null;

  pointerPosition = null;

  // countries in the john hopkins dataset have diffrent names than in countries-50m.json
  // this mapping translates from john hopkins to countries-50m
  // data that exist in jh isnt a country is treated as NOT_A_COUNTRY

  constructor() {
    autoPlay(true); // sets up tween animation loop
  }

  // -------------------- utility functions
  static getRandom(min, max) {
    return Math.random() * (max - min + 1) + min;
  }

  // select random element from array
  static selectRandom(a) {
    const index = Math.floor(Globe.getRandom(0, a.length - 1));
    return a[index];
  }

  static rollDice(cases, total) {
    return Globe.getRandom(0, total) < cases;
  }

  static inside(point, vs) {
    // checks if point [long,lat] is inside polygon vs [[long, lat]]
    // todo use d3.geoContains instead
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

    const x = point[0];
    const
      y = point[1];

    let isInside = false;
    for (let i = 0, j = vs.length - 1; i < vs.length; j = i, i += 1) {
      const xi = vs[i][0];
      const yi = vs[i][1];
      const xj = vs[j][0];
      const yj = vs[j][1];

      const intersect = ((yi > y) !== (yj > y))
        && (x < ((xj - xi) * (y - yi)) / (yj - yi) + xi);
      if (intersect) isInside = !isInside;
    }

    return isInside;
  }

  static getBoundingBox(poly) {
    const longs = poly.map((x) => x[0]);
    const lats = poly.map((x) => x[1]);
    return {
      lat: {
        min: Math.min(...lats),
        max: Math.max(...lats),
      },
      long: {
        min: Math.min(...longs),
        max: Math.max(...longs),
      },
    };
  }

  static randomPointInPolygon(poly, boundingBox) {
    const bb = boundingBox || Globe.getBoundingBox(poly);
    const sample = () => [
      Globe.getRandom(bb.long.min, bb.long.max),
      Globe.getRandom(bb.lat.min, bb.lat.max),
    ];
    let point = sample();
    let tries = 0;
    while (!Globe.inside(point, poly)) {
      point = sample();
      tries += 1;
      if (tries > 1000) {
        console.log(poly, boundingBox);
        console.warn('unlucky or something went wrong');
        break;
      }
    }
    return point;
  }

  init() {
    // preprocess country data
    this.countryToPoly = {};
    function largestPolygon(coordinates) {
      const longestIndex = coordinates.reduce(
        (maxIndex, x, i) => (x[0].length > coordinates[maxIndex][0].length ? i : maxIndex), 0,
      );
      return coordinates[longestIndex][0];
    }
    topojson.feature(countryTopojson, countryTopojson.objects.countries)
      .features.forEach((x) => {
        const countryName = x.properties.name.toLowerCase();
        // note some countries are constructed of multiple polygons
        // (because of islands and other exclaves)
        // we are only considering the largest polygon in that case (for now at least)
        const { coordinates } = x.geometry;
        const poly = x.geometry.type === 'MultiPolygon' ? largestPolygon(coordinates) : coordinates[0];
        this.countryToPoly[countryName] = {
          name: x.properties.name,
          feature: x,
          poly,
          boundingBox: Globe.getBoundingBox(poly),
        };
      });

    // setup three
    const container = document.getElementById('container');
    const containerBounds = container.getBoundingClientRect();
    this.scene = new Three.Scene();
    this.camera = new Three.PerspectiveCamera(
      75,
      containerBounds.width / containerBounds.height,
      0.5,
      1000,
    );

    // New Renderer
    this.renderer = new Three.WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(containerBounds.width, containerBounds.height);
    container.appendChild(this.renderer.domElement);

    // Setup resize listener
    window.addEventListener('resize', () => {
      const canvasContainer = document.getElementById('container');
      const { width: w, height: h } = canvasContainer.getBoundingClientRect();
      this.camera.aspect = w / h;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(w, h);
    });

    this.controlsOrbit = new OrbitControls(this.camera, this.renderer.domElement);
    this.controlsOrbit.minDistance = 15;
    this.controlsOrbit.maxDistance = 40;
    this.controlsOrbit.autoRotate = store.state.globeAutorotation;
    this.controlsOrbit.maxPolarAngle = Math.PI;
    this.controlsOrbit.screenSpacePanning = true;

    let autorotateTimeout = 0;
    this.controlsOrbit.addEventListener('start', () => {
      clearTimeout(autorotateTimeout);
      this.controlsOrbit.autoRotate = false;
    });

    this.controlsOrbit.addEventListener('end', () => {
      autorotateTimeout = setTimeout(() => {
        this.controlsOrbit.autoRotate = store.state.globeAutorotation;
      }, 2000);
    });

    //
    this.canvas = this.renderer.domElement;
    this.canvas.addEventListener('touchstart', (e) => {
      this.handleMouseDown(e);
    });

    this.canvas.addEventListener('mousedown', (e) => {
      this.handleMouseDown(e);
    });

    this.canvas.addEventListener('touchend', (e) => {
      this.handleClick(e);
    });

    this.canvas.addEventListener('click', (e) => {
      this.handleClick(e);
    });

    this.planet = new Three.Object3D();

    // sphere with horizontal and vertical lines
    const geometry = new Three.SphereBufferGeometry(this.EARTH_RADIUS, 32, 32);
    const material = new Three.MeshBasicMaterial({
      transparent: true,
      opacity: 0.8,
      color: 0x000000,
    });
    const earth = new Three.Mesh(geometry, material);
    earth.name = 'earth'; // used to intersect with the raycaster

    this.planet.add(earth);
    const edgesGeometry = new Three.EdgesGeometry(geometry);
    const wireframe = new Three.LineSegments(
      edgesGeometry,
      new Three.LineBasicMaterial({ color: 0x333333, transparent: true }),
    );
    this.planet.add(wireframe);
    this.scene.add(this.planet);
    // ADD COUNTRIES
    this.addCountries(this.scene);

    // Set the camera position
    this.camera.position.z = 2.5 * this.EARTH_RADIUS;

    // rayCaster for click handler
    this.rayCaster = new Three.Raycaster();

    // light
    this.addLight();

    // if country is already sleceted move flag
    if (store.state.selectedCountry) {
      this.moveListeningPositionIndicator(store.state.selectedCountry);
    }

    // Render the image
    const render = () => {
      this.controlsOrbit.update();
      requestAnimationFrame(render);
      this.renderer.render(this.scene, this.camera);

      // This is where we sample where spheres should be displayed
      // if VISUALISATION_SAMPLING_FREQUENCY is e.g. 0.9 we sample every 10th frame on average
      if (Math.random() > this.VISUALISATION_SAMPLING_FREQUENCY) {
        this.pulseRandomSpheresInCountriesAccordingToData();
      }
      if (store.state.useRadius === false && this.currentRadius) {
        this.scene.remove(this.currentRadius);
        this.currentRadius = undefined;
      }
      if (store.state.useRadius && this.userPosition) {
        this.drawRadius(store.state.radius, this.COLOR_RADIUS);
      }
    };
    render();
  }

  getCountryForLatLong(lat, long) {
    let res = 'Ocean';
    Object.values(this.countryToPoly).forEach((c) => {
      if (geoContains(c.feature, [long, lat])) {
        res = c.name;
      }
    });
    return res;
  }


  addFlag(poleHeight, countryName) {
    const isoCode = nameResolver.mapToIsocode(countryName);
    new Three.TextureLoader().load(`/img/flags/png1000px/${isoCode}.png`, (texture) => {
      const flagWidth = 1;
      const flagHeight = texture.image.height / (texture.image.width * flagWidth);
      const material = new Three.MeshLambertMaterial({ map: texture, side: Three.DoubleSide });

      const plane = new Three.Mesh(new Three.PlaneGeometry(flagWidth, flagHeight), material);
      plane.scale.x = -1;
      const heighAdjustment = poleHeight / 2 - flagHeight / 2; // poleHeight/2 - flagHeight/2
      plane.translateOnAxis(new Three.Vector3(-flagWidth / 2, heighAdjustment, 0), 1);
      this.userPosition.add(plane);
    });
  }

  moveListeningPositionIndicator({ lat, lon, countryName }) {
    if (this.userPosition) this.scene.remove(this.userPosition);
    this.userPosition = new Three.Object3D();

    // Flag pole
    const poleRadius = 0.05;
    const poleHeight = 1.5;
    const geometry = new Three.CylinderGeometry(poleRadius, poleRadius, poleHeight, 32);
    const material = new Three.MeshPhongMaterial({ color: 0xb89375 });
    const cylinder = new Three.Mesh(geometry, material);
    this.userPosition.add(cylinder);

    // move to position on globe and rotate up right
    const cartesianPoint = this.toPoint([lon, lat]);
    this.userPosition.translateOnAxis(cartesianPoint, 1 + (poleHeight / 2) / this.EARTH_RADIUS);
    this.userPosition.lookAt(new Three.Vector3(0, 0, 0));
    this.userPosition.rotateOnAxis(new Three.Vector3(1, 0, 0), 1.5 * Math.PI);

    this.scene.add(this.userPosition);
    this.addFlag(poleHeight, countryName);
  }

  // adds an ambient and a spotlight
  addLight() {
    this.scene.add(new Three.AmbientLight(0x505050, 2));
    const light = new Three.PointLight(0x505050, 2);
    light.position.x = 10;
    light.position.y = 10;
    this.camera.add(light);
    this.scene.add(this.camera);
  }

  getClicked3DPoint(pointerPosition) {
    this.rayCaster.setFromCamera(pointerPosition, this.camera);
    const intersects = this.rayCaster.intersectObjects(
      [this.scene.getObjectByName('earth')],
      true,
    );
    if (intersects.length > 0) return intersects[0];
    return undefined;
  }

  getPointerPosition(evt) {
    evt.preventDefault();
    let clientX;
    let clientY;
    if (evt.type === 'touchstart' || evt.type === 'touchend') {
      clientX = evt.changedTouches[0].clientX;
      clientY = evt.changedTouches[0].clientY;
    } else {
      clientX = evt.clientX;
      clientY = evt.clientY;
    }
    const pointerPosition = new Three.Vector2();
    const canvasPosition = this.canvas.getBoundingClientRect();
    const canvasWidth = this.canvas.width / devicePixelRatio;
    const canvasHeight = this.canvas.height / devicePixelRatio;

    pointerPosition.x = ((clientX - canvasPosition.left) / canvasWidth) * 2 - 1;
    pointerPosition.y = -((clientY - canvasPosition.top) / canvasHeight) * 2 + 1;
    return pointerPosition;
  }

  handleMouseDown(e) {
    if (!this.enabled) return;
    e.preventDefault();
    this.pointerPosition = this.getPointerPosition(e);
  }

  handleClick(e) {
    if (!this.enabled) return;
    e.preventDefault();

    const pointerPosition = this.getPointerPosition(e);

    if (Math.abs(this.pointerPosition.x - pointerPosition.x) > 0.01 // threshold
      || Math.abs(this.pointerPosition.y - pointerPosition.y) > 0.01) return;

    const target = this.getClicked3DPoint(pointerPosition);
    if (!target) return;

    const { x, y, z } = target.point;
    const spherical = new Three.Spherical();
    spherical.setFromCartesianCoords(x, y, z);

    const toDegrees = (rad) => (rad * 180) / Math.PI;
    const lat = 90 - toDegrees(spherical.phi);
    const lon = toDegrees(spherical.theta);

    console.debug('theta (left-right, longitude)', toDegrees(spherical.theta));
    console.debug('phi (up-down, latitude) deg', 90 - toDegrees(spherical.phi));

    const countryName = this.getCountryForLatLong(lat, lon);
    // dispatch new country selected
    store.dispatch(actions.LOAD_COUNTRY, {
      lon,
      lat,
      countryName,
    });
    this.moveListeningPositionIndicator({ lat, lon, countryName });
  }

  static getUnitSphere(point, color) {
    const s = new Three.Object3D();
    const geometry = new Three.SphereGeometry(1, 12, 12);
    const material = new Three.MeshPhysicalMaterial({ color: color || 'green' });
    const sphere = new Three.Mesh(geometry, material);

    s.add(sphere);
    s.translateOnAxis(point, 1);
    return s;
  }

  toPoint([long, lat]) {
    const lambda = (long * Math.PI) / 180;
    const phi = (lat * Math.PI) / 180;
    const cosPhi = Math.cos(phi);

    return new Three.Vector3(
      this.EARTH_RADIUS * cosPhi * Math.sin(lambda),
      this.EARTH_RADIUS * Math.sin(phi),
      this.EARTH_RADIUS * cosPhi * Math.cos(lambda),
    );
  }

  // Converts a GeoJSON MultiLineString in spherical coordinates to a THREE.LineSegments
  countriesToWireframe(multilinestring, material) {
    const geometry = new Three.Geometry();
    multilinestring.coordinates.forEach((line) => {
      d3.pairs(line.map((l) => this.toPoint(l)), (a, b) => {
        geometry.vertices.push(a, b);
      });
    });
    return new Three.LineSegments(geometry, material);
  }

  addCountries() {
    const mesh = this.countriesToWireframe(
      topojson.mesh(countryTopojson, countryTopojson.objects.countries),
      new Three.MeshBasicMaterial({ color: 0xffc0cb }),
    );
    this.scene.add(mesh);
  }

  pulseSphere(point, from, to, color) {
    const sphere = Globe.getUnitSphere(point, color);
    sphere.visible = false;
    this.scene.add(sphere);

    const shrinkAndDisappear = new Tween({ scale: to })
      .to({ scale: from }, this.animationDuration / 2)
      .on('update', ({ scale }) => {
        sphere.scale.set(scale, scale, scale);
      })
      .on('complete', () => {
        this.scene.remove(sphere);
      });

    // scale out
    new Tween({ scale: from })
      .to({ scale: to }, this.animationDuration / 2)
      .easing(Easing.Quadratic.In)
      .on('update', ({ scale }) => {
        sphere.visible = true;
        sphere.scale.set(scale, scale, scale);
      })
      .on('complete', () => shrinkAndDisappear.start())
      .start();
  }

  // pulses a random spher within the largest polygon of the given country
  // if the random point is outsied of the radius selected radius (when enabled)
  // nothing is displayed
  pulseRandomSphereInCountry(name, color) {
    const country = this.countryToPoly[name];
    if (!country) {
      console.error(`Country ${name} in john hopkins dataset does not match any of the countries-50m.json countries`);
      return;
    }
    const longLat = Globe.randomPointInPolygon(country.poly, country.boundingBox);
    if (store.state.useRadius) {
      const { lon, lat } = store.state.selectedCountry;
      if (d3.geoDistance(longLat, [lon, lat]) * 6701 > Math.exp(store.state.radius)) {
        // sphere outside of radius
        return;
      }
    }
    const cartesianPoint = this.toPoint(longLat);
    this.pulseSphere(cartesianPoint, 0, 0.3, color);
    this.expandCircle(cartesianPoint, color);
  }

  pulseRandomSphere() {
    const lat = Globe.getRandom(-90, 90);
    const long = Globe.getRandom(-180, 180);
    const cartesianPoint = this.toPoint([long, lat]);
    const color = Globe.selectRandom(['blue', 'green', 'yellow']);
    this.pulseSphere(cartesianPoint, 0, 0.3, color);
    this.expandCircle(cartesianPoint, color);
  }

  // for each country we compute if we should show a sphere probabilistically
  // example:
  // the chance for country c to display a value for a death is c.deaths/maxTotalNumber
  // where maxTotalNumber is the maximum number of cases in any of the days
  // (max of (deaths + recovered + confirmed))
  pulseRandomSpheresInCountriesAccordingToData() {
    // If there is no data or the music is not playing, don't do anything
    if (store.state.country.length === 0 || !store.state.playing) return;

    const { data } = store.state.country[store.state.currentDataIndex];
    const { maxTotalNumber } = store.getters;
    data.forEach((country) => {
      const name = nameResolver.resolveCountryNameFromJH(country.country);
      if (name === nameResolver.NOT_A_COUNTRY) return;
      if (Globe.rollDice(country.deaths, maxTotalNumber)) {
        this.pulseRandomSphereInCountry(name, this.COLOR_DEAD);
      }
      if (Globe.rollDice(country.confirmed, maxTotalNumber)) {
        this.pulseRandomSphereInCountry(name, this.COLOR_CONFIRMED);
      }
      if (Globe.rollDice(country.recovered, maxTotalNumber)) {
        this.pulseRandomSphereInCountry(name, this.COLOR_RECOVERED);
      }
    });
  }

  drawRadius(radius, color) {
    const thRadians = (Math.exp(radius) * 360) / 40040;
    const circleRadius = this.EARTH_RADIUS * Math.sin(thRadians * (Math.PI / 180));
    const distFromCenter = this.EARTH_RADIUS * Math.cos(thRadians * (Math.PI / 180));

    const { position } = this.userPosition;
    const unitVector = {
      x: position.x / this.EARTH_RADIUS,
      y: position.y / this.EARTH_RADIUS,
      z: position.z / this.EARTH_RADIUS,
    };
    let circle;
    if (!this.currentRadius) {
      circle = new Object3D();
    } else {
      this.scene.remove(this.currentRadius);
      circle = new Object3D();
    }
    const geometry = new Three.CircleGeometry(circleRadius, 100);
    const edgesGeometry = new Three.EdgesGeometry(geometry);
    const material = new Three.LineBasicMaterial({ color, linewidth: 20, transparent: false });
    const wireframe = new Three.LineSegments(
      edgesGeometry,
      material,
    );

    const orientation = Math.exp(radius) > 10000 ? 1 : -1;
    const diff = 0.5 * orientation;

    circle.add(wireframe);
    circle.lookAt(this.userPosition.position);
    circle.position.set(
      unitVector.x * (distFromCenter + diff),
      unitVector.y * (distFromCenter + diff),
      unitVector.z * (distFromCenter + diff),
    );

    this.scene.add(circle);
    this.currentRadius = circle;
  }

  // expands a circle around the given point on the globe
  // (also fades by becoming less transparent)
  expandCircle(point, color) {
    const circle = new Object3D();
    const geometry = new Three.CircleGeometry(1, 32);
    const edgesGeometry = new Three.EdgesGeometry(geometry);
    const material = new Three.LineBasicMaterial({ color, linewidth: 10, transparent: true });
    const wireframe = new Three.LineSegments(
      edgesGeometry,
      material,
    );

    circle.add(wireframe);
    this.scene.add(circle);

    new Tween({ ratioOfRadius: 1, opacity: 1 })
      .to({ ratioOfRadius: 0.9, opacity: 0 }, this.animationDuration)
      .easing(Easing.Quadratic.In)
      .on('update', ({ ratioOfRadius, opacity }) => {
        const distFromCenter = ratioOfRadius * this.EARTH_RADIUS;
        const circleRadius = Math.sqrt(
          this.EARTH_RADIUS * this.EARTH_RADIUS - distFromCenter * distFromCenter,
        );
        // adding a small number (0.005) to make the circle show up over the country lines
        const newPosition = point.clone().multiplyScalar(ratioOfRadius + 0.005);
        circle.position.set(newPosition.x, newPosition.y, newPosition.z);
        circle.lookAt(new Three.Vector3(0, 0, 0));
        circle.scale.set(circleRadius, circleRadius, circleRadius);
        material.opacity = opacity;
      })
      .on('complete', () => this.scene.remove(circle))
      .start();
  }

  setEnabled(flag) {
    this.enabled = flag;
    this.controlsOrbit.enabled = flag;
  }

  setAutorotation(flag) {
    this.controlsOrbit.autoRotate = flag;
  }
}
