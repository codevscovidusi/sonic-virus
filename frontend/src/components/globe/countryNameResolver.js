/**
 * This file is used to translate between diffrent representations of country names
 */
import countriesToIso from './countriesToIso.json';

// did not add flags for "Siachen Glacier", "N. Cyprus" and "Somaliland"
// when a country is not found or the ocean is clicked un_flag is returned
const mapToIsocode = (name) => countriesToIso[name] || 'un_flag';

const NOT_A_COUNTRY = 'NOT_A_COUNTRY';

const johnHopkinsToGeoJson = {
  'antigua and barbuda': 'antigua and barb.',
  'bosnia and herzegovina': 'bosnia and herz.',
  'central african republic': 'central african rep.',
  'congo (brazzaville)': 'congo',
  'congo (kinshasa)': 'congo',
  "cote d'ivoire": "côte d'ivoire",
  'diamond princess': NOT_A_COUNTRY,
  'dominican republic': 'dominican rep.',
  'equatorial guinea': 'eq. guinea',
  eswatini: 'eswatini',
  'holy see': NOT_A_COUNTRY,
  'korea, south': 'south korea',
  'north macedonia': 'macedonia',
  'saint vincent and the grenadines': 'st. vin. and gren.',
  'taiwan*': 'taiwan',
  us: 'united states of america',
  'west bank and gaza': 'israel',
  'saint kitts and nevis': 'st. kitts and nevis',
  burma: 'myanmar',
  'ms zaandam': NOT_A_COUNTRY,
};

const resolveCountryNameFromJH = (name) => johnHopkinsToGeoJson[name] || name;

export default {
  mapToIsocode,
  resolveCountryNameFromJH,
  NOT_A_COUNTRY,
};
